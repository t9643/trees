import pandas as pd
import math
df = pd.read_csv("cats.csv")
df = df.iloc[:, 1:]
print(df)

# x - название столбца признака, Например x = Гавкает
def entropy_cat_dog(x):
    dog_0 = 0
    cat_0 = 0
    dog_1 = 0
    cat_1 = 0

    # Разбиваем на две группы и считаем сколько всего собачек и котиков в каждой группе
    # Группа с единичками: dog_1 и cat_1
    # Группа с ноликами: dog_0 и cat_0
    for i in range(len(df)):
        if df["{}".format(x)][i] == 0:
            if df["Вид"][i] == "собачка":
                dog_0 += 1
            else:
                cat_0 += 1
        else:
            if df["Вид"][i] == "собачка":
                dog_1 += 1
            else:
                cat_1 += 1

    # Считаем энтропию двух групп. После того как разбили на две группы.
    if dog_0 == 0 or cat_0 == 0:
        E_0 = 0
    else:
        E_0 = - (dog_0 / (dog_0 + cat_0)) * math.log2(dog_0 / (dog_0 + cat_0)) - (cat_0 / (dog_0 + cat_0)) * \
        math.log2(cat_0 / (dog_0 + cat_0))

    if dog_1 == 0 or cat_1 == 0:
        E_1 = 0
    else:
        E_1 = -(dog_1 / (dog_1 + cat_1)) * math.log2(dog_1 / (dog_1 + cat_1)) - (cat_1 / (dog_1 + cat_1)) * \
        math.log2(cat_1 / (dog_1 + cat_1))

    # Подсчитали энтропию Y - Вид
    E_Y = -(4/10) * math.log2(4/10) - (6/10) * math.log2(6/10)

    # Считаем IG (information gain)
    all_cats_dogs = dog_0 + cat_0 + dog_1 + cat_1
    IG = E_Y - ((dog_1 + cat_1) / all_cats_dogs * E_1 + (dog_0 + cat_0) / all_cats_dogs * E_0)

    return "Entropy: {}, {} \n IG:{}".format(E_1, E_0,IG)

print(entropy("Гавкает"))




