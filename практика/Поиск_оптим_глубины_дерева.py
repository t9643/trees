from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split

train_iris = pd.read_csv("train_iris.csv")
print(train_iris.head())

# Подсчитмаем пропущенные значения в переменных
print(train_iris.isnull().sum())
print(train_iris.iloc[:,0].max())
print(train_iris.shape[0])

# Разделяем дата фрейм на признаки, котоые будут предсказывать Y
X = train_iris.drop(['species'])
Y = train_iris["species"]

# Разбиваем множество на тренировоные данные и тестовые


