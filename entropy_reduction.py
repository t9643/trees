from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.DataFrame({'X_1': [1, 1, 1, 0, 0, 0, 0, 1], 'X_2': [0, 0, 0, 1, 0, 0, 0, 1], 'Y': [1, 1, 1, 1, 0, 0, 0, 0]})
print(data)

# Сожаем дерево решений
clf = tree.DecisionTreeClassifier(criterion = "entropy")
print(clf)

# Отдельно сохраняем пременные x и y
X = data[["X_1", "X_2"]] # Исходные данные
y = data.Y # Предсказанные данные

# Обучим наше дерево решений
print(clf.fit(X, y))

