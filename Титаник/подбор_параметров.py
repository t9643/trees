from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_score, recall_score

# Настроки вывода дата фрейма. Показываем все столбцы без переноса.
pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

titanic_data = pd.read_csv("train.csv")
X = titanic_data.drop(["PassengerId", "Survived", "Name", "Ticket", "Cabin"], axis = 1)
Y = titanic_data.Survived
X = pd.get_dummies(X)
#Заполняем пропуски в Age Средним (медиана) значением
X = X.fillna({'Age':X.Age.median()})

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.33, random_state = 42)

clf = tree.DecisionTreeClassifier()

parametrs = {'criterion': ['gini', 'entropy'], 'max_depth': range(1, 30)}

grid_search_cv_clf = GridSearchCV(clf, parametrs, cv = 5)
grid_search_cv_clf.fit(X_train, y_train)
print(grid_search_cv_clf.best_params_)

best_clf = grid_search_cv_clf.best_estimator_
print(best_clf)

print(best_clf.score(X_test, y_test))

# Рассчитываем precision and recall
y_pred = best_clf.predict(X_test)
print(f"{precision_score(y_test, y_pred)} - Precision")
print(f"{recall_score(y_test, y_pred)} - Recall \n")

print(f"{y_pred} \n")

y_predicted_prob = best_clf.predict_proba(X_test)
print(f"{y_predicted_prob} \n")

pd.Series(y_predicted_prob[:, 1]).hist()
print(plt.show())

import numpy as np
y_pred = np.where(y_predicted_prob[:, 1] > 0.9, 1, 0)
print(precision_score(y_test, y_pred))
print(recall_score(y_test, y_pred))
