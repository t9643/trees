import pandas as pd
from sklearn import tree
import matplotlib.pyplot as plt

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split

pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

data_train_tree = pd.read_csv("train_data_tree.csv")

print(data_train_tree.isnull().sum())
print(data_train_tree.head(100))

X_train = data_train_tree.drop(['num'], axis = 1)
Y_train = data_train_tree.num
Y_train = pd.DataFrame(Y_train, columns = ['num'])

# X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.33, random_state = 0)

clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = 4)
'''
parametrs = {'criterion': ['entropy'], 'max_depth': range(1, 50)}
grid_search_cv_clf = GridSearchCV(clf, parametrs, cv = 5)
grid_search_cv_clf.fit(X_train, Y_train)
print(grid_search_cv_clf.best_params_)

best_clf = grid_search_cv_clf.best_estimator_
'''

clf.fit(X_train, Y_train)

# Индексы листьев
l_node = clf.tree_.children_left[0]
R_node = clf.tree_.children_right[0]

# samples этих листьев
samples_right = clf.tree_.n_node_samples[R_node]
samples_left = clf.tree_.n_node_samples[l_node]
samples_root = clf.tree_.n_node_samples[0]

# Этропия листьев
e_l = clf.tree_.impurity[l_node]
e_r = clf.tree_.impurity[R_node]
e_root = clf.tree_.impurity[0]

IG = e_root - (samples_left/samples_root*e_l +
               samples_right/samples_root*e_r)

print(f"Infomation Gain is {IG}")


tree.plot_tree(clf, filled=True)
print(plt.show())

