from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

data_pets = pd.read_csv("dogs_n_cats.csv")
print(data_pets.head(20))

X_train = data_pets.drop('Вид', axis = 1)
Y_train = data_pets.Вид

#Convert string to number
Y_train = pd.get_dummies(Y_train)
Y_train = Y_train.drop('котик', axis = 1)
# If it is dog - 1, cat - 0
Y_train.rename(columns={'собачка':'Вид'}, inplace = True)
# Check empty cells
print(data_pets.isnull().sum())

X_train, X_test, Y_train, Y_test = train_test_split(X_train, Y_train, test_size = 0.33, random_state = 0)

max_depth_values = range(1, 50)
score_data = pd.DataFrame()

for max_depth in max_depth_values:
    clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = max_depth)
    mean_cross_val_score = cross_val_score(clf, X_train, Y_train, cv = 5).mean()
    clf.fit(X_train, Y_train)
    train_score = clf.score(X_train, Y_train)
    test_score = clf.score(X_test, Y_test)
    temp_score_data = pd.DataFrame({"max_depth": [max_depth],
                                    "train_score": [train_score],
                                    "test_score": [train_score],
                                    "cross_val_score": [mean_cross_val_score]})
    score_data = score_data.append(temp_score_data)

print(score_data.head(20))
score_data_long = pd.melt(score_data, id_vars = ["max_depth"],
                          value_vars = ['train_score', 'test_score', 'cross_val_score'],
                          var_name = "set_type",
                          value_name = "score")

print(score_data_long.head(20))

sns.lineplot(data = score_data_long, x = "max_depth", y = "score", hue = 'set_type')
print(plt.show())

# Загружаем новый датасет для предсказания, что это собака или кошка на основе данных признаков
data_predict = pd.read_json(r"dataset_209691_15.txt")
best_clf = tree.DecisionTreeClassifier(criterion  ='entropy', max_depth=1)
best_clf.fit(X_train, Y_train)

print(data_predict.head())
result = best_clf.predict(data_predict)
# Выводит количство собак - 1 и кошек - 0
print(result)
# Суммируем всех собак
dogs = pd.Series(result)[result == 1].count()
print(dogs)

