from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from IPython.display import SVG
from graphviz import Source
from IPython.display import display

from IPython.display import HTML

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

# Настроки вывода дата фрейма. Показываем все столбцы без переноса.
pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

titanic_data = pd.read_csv("train.csv")
X = titanic_data.drop(["PassengerId", "Survived", "Name", "Ticket", "Cabin"], axis = 1)
Y = titanic_data.Survived
X = pd.get_dummies(X)
#Заполняем пропуски в Age Средним (медиана) значением
X = X.fillna({'Age':X.Age.median()})

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.33, random_state = 42)
max_depth_values = range(1, 100)
score_data = pd.DataFrame()

for max_depth in max_depth_values:
    clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = max_depth)
    mean_cross_val_score = cross_val_score(clf, X_train, y_train, cv=5).mean()
    clf.fit(X_train, y_train)
    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)

    temp_score_data = pd.DataFrame({"max_depth": [max_depth],
                                    "train_score": [train_score],
                                    "test_score": [test_score],
                                    "cross_val_score": [mean_cross_val_score]})
    score_data = score_data.append(temp_score_data)

score_data_long = pd.melt(score_data, id_vars = ['max_depth'],
                          value_vars=['train_score', "test_score", 'cross_val_score'],
                          var_name = 'set_type', value_name = 'score')

print(score_data_long.head())

sns.lineplot(x = 'max_depth', y = 'score', hue = 'set_type', data = score_data_long)
print(plt.show())

print(score_data_long.query("set_type == 'cross_val_score'").head(20))

best_clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth = 8)

best_clf.fit(X_train, y_train)
print(best_clf.score(X_train, y_train))
print(best_clf.score(X_test, y_test))

# Строим график. Чему наше дерево решений научилось
graph = Source(tree.export_graphviz(best_clf, out_file = None,
                                    feature_names = list(X), class_names = ["Died", "Survived"],
                                    filled = True))
display(SVG(graph.pipe(format = 'svg')))

plt.figure(figsize=(100, 50))
tree.plot_tree(clf,
               feature_names=list(X),
               class_names=['Died', 'Survived'],
               filled=True,
               rounded=True,
               fontsize=11)
print(plt.show())

