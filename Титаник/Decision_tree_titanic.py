from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from IPython.display import SVG
from graphviz import Source
from IPython.display import display

from IPython.display import HTML
style = "<style>svg{width: 70% !important; height: 60% !important;} </style>"
HTML(style)

# Настроки вывода дата фрейма. Показываем все столбцы без переноса.
pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

titanic_data = pd.read_csv("train.csv")

# Задача обучить дерево решений, чтобы предсказывать выжел пассажир или нет

# Для начало подсчитаем пропущенные значения в каждом столбце.
print(titanic_data.isnull().sum())
print(titanic_data.head())

# Отбросим id пассажира, Cabin (Номер каюты, слишком много пропусков) и Ticket (Номер билета -строковое значение)
# Так же имя пассажира. Ничего не даст. Поэтому можно отбросить.
# И дропаем Survived, так мы хотим предсказывать данныую переменную.
X = titanic_data.drop(["PassengerId", "Survived", "Name", "Ticket", "Cabin"], axis = 1)
Y = titanic_data['Survived'] # Сохраняем значение, которое необходимо предсказать.

# get_dummies конвертирует из строкового формата в числовой. Напривер столбец с female и male.
# Данный столбец просто разобьет на два столбца female и male. заполнит 1- да и 0 - нет.
X = pd.get_dummies(X)
print(X.head())

# У нас остались пропущенные занчения в столбцах Age. Заполних их медианным значением.
X = X.fillna({'Age': X.Age.median()})

# Достаем дерево решений. Скармливаем вводные данные и выходные данные
clf = tree.DecisionTreeClassifier(criterion = "entropy")
clf.fit(X, Y)

# Строим график. Чему наше дерево решений научилось
# graph = Source(tree.export_graphviz(clf, out_file = None,
#                                     feature_names = list(X), class_names = ["Died", "Survived"],
#                                     filled = True))
# display(SVG(graph.pipe(format = 'svg')))

# plt.figure(figsize=(100, 50))
# tree.plot_tree(clf,
#                feature_names=list(X),
#                class_names=['Died', 'Survived'],
#                filled=True,
#                rounded=True,
#                fontsize=11)
#
# print(plt.show())



from sklearn.model_selection import train_test_split
# Разбиваем множество на 0.33 и 0.66 соответственно. Чтобы обрезать дерево решений.
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.33, random_state=42)
print(X_train.shape)
print(X_test.shape)

# Метод score предсказывает самы простой показатель метрики качества модели - это число правильных решений
# Точночть получается 0.979797
print(clf.score(X, Y))

# Посмотрим точночть предсказывания на на трайн данных
# 0.9798
clf.fit(X_train, y_train)
print(clf.score(X_train, y_train))
# А теперь посмотрим какая у нас точность на данных test, которые классификатор не видел
# 0.78305
# Предсказывает хуже на тех данных, которые не видел. Наша модель переобучилась, стараясь определить частные случаи
print(clf.score(X_test, y_test))


# Поэтому мы говорим дереву ты не обращай внимание на выбрасы. Обрезаем дерево
clf = tree.DecisionTreeClassifier(criterion = "entropy", max_depth = 5)
clf.fit(X_train, y_train)
# 0,8406
print(clf.score(X_train, y_train))
# 0.8067
print(clf.score(X_test, y_test))


# Давай проверим точность предсказывания дерева в зависимости от длины дерева (глубины)
max_depth_values = range(1, 100)
score_data = pd.DataFrame()

for max_depth in max_depth_values:
    clf = tree.DecisionTreeClassifier(criterion = "entropy", max_depth = max_depth)
    clf.fit(X_train, y_train)
    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)

    temp_score_data = pd.DataFrame({'max_dapth': [max_depth],
                                    "train_score": [train_score],
                                    "test_score": [test_score]})
    score_data = score_data.append(temp_score_data)

print(score_data.head())

# Плавим получившийся датафрейм
scores_data_long = pd.melt(score_data, id_vars=['max_dapth'], value_vars = ["train_score", "test_score"],
                           var_name = "set_type", value_name = "score")
print(scores_data_long)

# ВИзуализируем данные
sns.lineplot(x="max_dapth", y="score", hue="set_type", data=scores_data_long)
print(plt.show())



# Тоже самое, только используя кросс валидацию. Когда train дата сет делим на несколько равных частей X.
# Одна часть становится тестовой, другие тернировочные. Потом меняются ролями пока все части не побудут тестовыми.
# Полится список из  X значений. Далле находится среднее значение
from sklearn.model_selection import cross_val_score
max_depth_values = range(1, 100)
score_data = pd.DataFrame()

for max_depth in max_depth_values:
    clf = tree.DecisionTreeClassifier(criterion="entropy", max_depth = max_depth)
    clf.fit(X_train, y_train)
    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)

    mean_cross_val_score = cross_val_score(clf, X_train, y_train, cv=5).mean()

    temp_score_data = pd.DataFrame({"max_depth":[max_depth],
                                    "train_score": [train_score],
                                    "test_score":[test_score],
                                    "cross_val_score":[mean_cross_val_score]})
    score_data = score_data.append(temp_score_data)

print(score_data.head())

score_data_long = pd.melt(score_data, id_vars=["max_depth"],
                          value_vars=["train_score", "test_score", "cross_val_score"],
                          var_name = "set_type", value_name="score")

sns.lineplot(x="max_depth", y="score", hue="set_type", data = score_data_long)
print(plt.show())

# Посмотрели на графике оптимальное значение, теперь посмотрим в таблице точное значение.Какое максимальное
# значение достигает переменная cross_val_score.
# При глубине дерева 11 точность cross_val_score достигает максимум = 0.800350
print(score_data_long.query("set_type == 'cross_val_score'").head(20))

# Когда потренировались на Train датасете. Окончательную проверку проделываем на test датасете.
best_clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth= 10)
print(cross_val_score(clf, X_test, y_test, cv=5).mean())

