from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from IPython.display import SVG
from graphviz import Source
from IPython.display import display

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score


# Настроки вывода дата фрейма. Показываем все столбцы без переноса.
pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

train_data = pd.read_csv("train_iris.csv", index_col = 0)
test_data = pd.read_csv("test_iris.csv", index_col = 0)

print(train_data.head())
print(test_data.head())

max_depth_value = range(1,100)
score_data = pd.DataFrame()

# Check empty cells
print(train_data.isnull().sum())
print(test_data.isnull().sum())

X_train = train_data.drop('species', axis = 1)
Y_train = train_data.species

X_test = test_data.drop('species', axis = 1)
Y_test = test_data.species
np.random.seed(0)
for max_depth in max_depth_value:
    clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = max_depth)
    mean_cross_val_score = cross_val_score(clf, X_train, Y_train, cv = 5).mean()
    clf.fit(X_train, Y_train)
    test_score = clf.score(X_test, Y_test)

    temp_score_data = pd.DataFrame({'max_depth':[max_depth], 'test_score':[test_score],
                                    'cross_val_score':[mean_cross_val_score]})
    score_data = score_data.append(temp_score_data)

score_data_long = pd.melt(score_data, id_vars = ['max_depth'], value_vars = ['cross_val_score', 'test_score'],
                          var_name = 'set_type', value_name = 'score')

print(score_data_long.head(10))

sns.lineplot(x = "max_depth", y = "score", hue = "set_type", data = score_data_long)
print(plt.show())

